-- ex. 2
-- extrai o conteúdo útil da lista de entrada
-- catMaybes :: [Maybe a] -> [a]
--
-- catMaybes [] = []
-- catMaybes (Just a):t = a:catMaybes(t)
-- catMaybes (Nothing:t) = catMaybes(t)

-- ex. 3
-- emparelha os argumentos de uma função
-- uncurry :: (a -> b -> c) -> (a, b) -> c
--
-- uncurry f(x,y) = fxy

-- inverso do uncurry
-- curry::((a,b) -> c) -> a -> b -> c

-- troca os argumentos de uma função
-- flip::(a -> b -> c) -> b -> a -> c

-- ex. 4
-- data LTree a = Leaf a | Fork (LTree a, LTree a)

-- lista de elementos da árvora argumento
-- flatten :: LTree a -> [a]

-- flatten (Leaf x) = [x]
-- flatten (Fork t1 t2) = (flatten t1)++(flatten b)

-- espelhar a árvvora argumento
-- mirror :: LTree a -> LTree a

-- mirror (Leaf x) = Leaf x
-- mirror (Fork t1 t2) = Fork (mirror t2) (mirror t2)

-- transformar cada folha x da árvore argumento na folha f x
-- fmap :: (b -> a) -> LTree b -> LTree a

-- ex. 5
