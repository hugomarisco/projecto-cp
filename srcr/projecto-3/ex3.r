library("neuralnet")
library("hydroGOF")

# Ler CSV
dataset <- read.csv("/Users/hugomarisco/Downloads/exercicio3.csv", header=TRUE, sep=",", dec=".")

# Definir dataset para testar a RNA
testeset <- dataset[400:500,]

# Construir a RNA
fadiganet <- neuralnet(FatigueLevel ~ Performance.KDTMean+Performance.MAMean+Performance.MVMean+Performance.TBCMean+Performance.DDCMean+Performance.DMSMean+Performance.AEDMean+Performance.ADMSLMean, dataset, hidden = 3)

# Desenha-la
plot(fadiganet)

# Retirar os dados relativos ao teste
temp_teste <- subset(testeset, select = c(Performance.KDTMean,Performance.MAMean,Performance.MVMean,Performance.TBCMean, Performance.DDCMean,Performance.DMSMean ,Performance.AEDMean, Performance.ADMSLMean))

# Calcular resultados
fadiganet.results <- compute(fadiganet, temp_teste)

# Extrair frame comparativo (actual, prediction)
actual_vs_prediction <- data.frame(actual = testeset$FatigueLevel, prediction = round(fadiganet.results$net.result))

# Extrair vector com as previsoes
predictions <- round(fadiganet.results$net.result)

# Criar matriz para colocar os resultados de fatigado/nao fatigado
sim_ou_nao <- matrix(, nrow = nrow(predictions), ncol = 2)

# Iterar e preencher a matriz
for (i in 1:nrow(predictions)) {
  sim_ou_nao[i,1] <- predictions[i,]
  if (predictions[i,]<5) sim_ou_nao[i,2]<-0 else sim_ou_nao[i,2]<-1
}

# Calcular o RMSE (erro médio)
rmse(c(actual_vs_prediction$actual),c(actual_vs_prediction$prediction))

