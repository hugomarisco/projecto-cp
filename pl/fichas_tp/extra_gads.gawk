BEGIN { nome_maior=""; valor_maior=0; counter=0; last_file=""; }
{
  if (last_file!=FILENAME) {
    if (counter>valor_maior) {
      nome_maior=last_file;
      valor_maior=counter;
    }
    last_file=FILENAME;
  } else {
    counter+=length($0)
  }
}
END { print "O maior ficheiro é o " nome_maior " com " valor_maior " bytes!" }
