#include <GLUT/glut.h>
#include <math.h>
#include <stdlib.h>

float altura=1.0, aresta=1.0, anguloRotacao = 0.0, leftRight = 0.0, frontBack = 0.0;

void changeSize(int w, int h) {
    
    // Prevent a divide by zero, when window is too short
    // (you cant make a window with zero width).
    if(h == 0) h = 1;
    
    // compute window's aspect ratio
    float ratio = w * 1.0 / h;
    
    // Set the projection matrix as current
    glMatrixMode(GL_PROJECTION);
    // Load Identity Matrix
    glLoadIdentity();
    
    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);
    
    // Set perspective
    gluPerspective(45.0f ,ratio, 1.0f ,1000.0f);
    
    // return to the model view matrix mode
    glMatrixMode(GL_MODELVIEW);
}

void referencial(int x, int y, int z) {
    glBegin(GL_LINES);
    
    glColor3f(1,0,0);
    glVertex3f(x, 0, 0);
    glVertex3f(-x, 0, 0);
    glColor3f(0,1,0);
    glVertex3f(0, y, 0);
    glVertex3f(0, -y, 0);
    glColor3f(0,0,1);
    glVertex3f(0, 0, z);
    glVertex3f(0, 0, -z);
    glColor3f(0,0,0);
    glEnd();
}


void renderScene(void) {
    
    // clear buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // set the camera
    glLoadIdentity();
    gluLookAt(3.0,2.0,5.0,
              0.0,1.0,0.0,
              0.0f,1.0f,0.0f);
    glRotatef(anguloRotacao, 0.0, 1.0, 0.0);
    glTranslatef(leftRight,0,frontBack);
    
    // pÃ´r instruÃ§Ãµes de desenho aqui
    
    referencial(5,5,5);
    
    // base
    glBegin(GL_TRIANGLES);
    glColor3f(1,0,0);
    glVertex3f(-(aresta/2), 0.0f, aresta/2);
    glVertex3f(-(aresta/2), 0.0f, -(aresta/2));
    glVertex3f(aresta/2, 0.0f, aresta/2);
    glEnd();
    
    glBegin(GL_TRIANGLES);
    glVertex3f(aresta/2, 0.0f, aresta/2);
    glVertex3f(-(aresta/2), 0.0f, -(aresta/2));
    glVertex3f(aresta/2, 0.0f, -(aresta/2));
    glEnd();
    
    // face 1
    glBegin(GL_TRIANGLES);
    glColor3f(0,1,0);
    glVertex3f(-(aresta/2), 0.0f, aresta/2);
    glVertex3f(aresta/2, 0.0f, aresta/2);
    glVertex3f(0.0f, altura, 0.0f);
    glEnd();
    
    // face 2
    glBegin(GL_TRIANGLES);
    glColor3f(0,0,1);
    glVertex3f(aresta/2, 0.0f, aresta/2);
    glVertex3f(aresta/2, 0.0f, -(aresta/2));
    glVertex3f(0.0f, altura, 0.0f);
    glEnd();
    
    // face 3
    glBegin(GL_TRIANGLES);
    glColor3f(1,1,0);
    glVertex3f(aresta/2, 0.0f, -(aresta/2));
    glVertex3f(-(aresta/2), 0.0f, -(aresta/2));
    glVertex3f(0.0f, altura, 0.0f);
    glEnd();
    
    // face 4
    glBegin(GL_TRIANGLES);
    glColor3f(1,0,1);
    glVertex3f(-(aresta/2), 0.0f, -(aresta/2));
    glVertex3f(-(aresta/2), 0.0f, aresta/2);
    glVertex3f(0.0f, altura, 0.0f);
    glEnd();
    
    // End of frame
    glutSwapBuffers();
}



// escrever funÃ§Ã£o de processamento do teclado
void registSpecialKeys(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT: anguloRotacao -= 5.0; break;
        case GLUT_KEY_RIGHT: anguloRotacao += 5.0; break;
        case GLUT_KEY_UP: altura += 1.0; break;
        case GLUT_KEY_DOWN: altura -= 1.0; break;
    }
}

void registKeys(unsigned char key, int x, int y) {
    switch(key) {
        case 27: exit(0);
        case 97: leftRight -= 1; break;
        case 100: leftRight += 1; break;
        case 115: frontBack += 1; break;
        case 119: frontBack -= 1; break;
    }
}

// escrever funÃ§Ã£o de processamento do menu

void menu(int op) {
    switch(op) {
        case 1: glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); break;
        case 2: glPolygonMode(GL_FRONT_AND_BACK,GL_LINE); break;
        case 3: glPolygonMode(GL_FRONT_AND_BACK,GL_POINT); break;
    }
    
}



int main(int argc, char **argv) {
    
    // inicializaÃ§Ã£o
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(800,800);
    glutCreateWindow("CG@DI-UM");
    
    
    // registo de funÃ§Ãµes
    glutDisplayFunc(renderScene);
    glutIdleFunc(renderScene);
    glutReshapeFunc(changeSize);
    
    // pÃ´r aqui registo da funÃ§Ãµes do teclado e rato
    glutSpecialFunc(registSpecialKeys);
    glutKeyboardFunc(registKeys);
    
    
    // pÃ´r aqui a criaÃ§Ã£o do menu
    glutCreateMenu(menu);
    glutAddMenuEntry("FILL", 1);
    glutAddMenuEntry("LINE", 2);
    glutAddMenuEntry("POINT",3);
    
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    
    // alguns settings para OpenGL
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    // entrar no ciclo do GLUT
    glutMainLoop();
    
    return 1;
}