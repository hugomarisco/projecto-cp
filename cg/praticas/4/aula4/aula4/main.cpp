#include <stdlib.h>
#include <stdio.h>
#include <GLUT/glut.h>

#include <math.h>

#define _PI_ 3.14159

float alpha = 0.0f, beta = 0.0f, radius = 5.0f;
float camX, camY, camZ;
GLuint buffer[1];
int n_pontos, frame=0, times, timebase, fps=0;
char print[20];

// declare variables for VBO id
//...

void sphericalToCartesian() {
    
    camX = radius * cos(beta) * sin(alpha);
    camY = radius * sin(beta);
    camZ = radius * cos(beta) * cos(alpha);
}


void changeSize(int w, int h) {
    
    // Prevent a divide by zero, when window is too short
    // (you cant make a window with zero width).
    if(h == 0)
        h = 1;
    
    // compute window's aspect ratio
    float ratio = w * 1.0 / h;
    
    // Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    // Set the viewport to be the entire window
    glViewport(0, 0, w, h);
    
    // Set the correct perspective
    gluPerspective(45,ratio,1,1000);
    
    // return to the model view matrix mode
    glMatrixMode(GL_MODELVIEW);
}


/*-----------------------------------------------------------------------------------
	Drawing cylinder with VBOs
 -----------------------------------------------------------------------------------*/

void drawCylinder() {
    
    //	Bind and semantics
    glBindBuffer(GL_ARRAY_BUFFER,buffer[0]);
    glVertexPointer(3,GL_FLOAT,0,0);
    
    //  Draw
    glDrawArrays(GL_TRIANGLES, 0, n_pontos);
}

/*-----------------------------------------------------------------------------------
	Create the VBO for the cylinder
 -----------------------------------------------------------------------------------*/

void prepareCylinder(float altura,float radius,int lados) {
    
    //	Enable buffer
    glEnableClientState(GL_VERTEX_ARRAY);
    
    // Allocate and fill vertex array
    float f1=0, f2=0;
    float angulo=2*_PI_/lados;
    n_pontos = 4*lados*3*3;
    float *v = (float*) malloc(n_pontos*sizeof(float));
    
    for (int i=0; lados>0; lados--) {
        f1=f2; f2+=angulo;
        
        //Base Superior
        v[i++]=0; v[i++]=altura; v[i++]=0;
        v[i++]=sin(f1); v[i++]=altura; v[i++]=cos(f1);
        v[i++]=sin(f2); v[i++]=altura; v[i++]=cos(f2);
        
        //Base Inferior
        v[i++]=0; v[i++]=0; v[i++]=0;
        v[i++]=sin(f2); v[i++]=0; v[i++]=cos(f2);
        v[i++]=sin(f1); v[i++]=0; v[i++]=cos(f1);
        
        
        //Dois triângulos para os lados
        v[i++]=sin(f1); v[i++]=altura; v[i++]=cos(f1);
        v[i++]=sin(f1); v[i++]=0; v[i++]=cos(f1);
        v[i++]=sin(f2); v[i++]=0; v[i++]=cos(f2);
        
        v[i++]=sin(f1); v[i++]=altura; v[i++]=cos(f1);
        v[i++]=sin(f2); v[i++]=0; v[i++]=cos(f2);
        v[i++]=sin(f2); v[i++]=altura; v[i++]=cos(f2);
    }
    
    // Generate VBOs
    
    glGenBuffers(1, buffer);
    
    glBindBuffer(GL_ARRAY_BUFFER, *buffer);
    
    glBufferData(GL_ARRAY_BUFFER, n_pontos*sizeof(float), v, GL_STATIC_DRAW);
    
    free(v);
}

/*-----------------------------------------------------------------------------------
 RENDER SCENE
 -----------------------------------------------------------------------------------*/

void renderScene(void) {
    
    float pos[4] = {1.0, 1.0, 1.0, 0.0};
    
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glLoadIdentity();
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
    gluLookAt(camX,camY,camZ,
              0.0,0.0,0.0,
              0.0f,1.0f,0.0f);
    
    drawCylinder();
    
    frame++;
    times=glutGet(GLUT_ELAPSED_TIME);
    if (times - timebase > 1000) {
        fps = frame*1000.0/(times-timebase);
        timebase = times;
        frame = 0;
    }
    sprintf(print, "%d",fps);
    glutSetWindowTitle(print);
    
    // End of frame
    glutSwapBuffers();
}


// special keys processing function
void processKeys(int key, int xx, int yy)
{
    switch(key) {
            
        case GLUT_KEY_RIGHT:
            alpha -=0.1; break;
            
        case GLUT_KEY_LEFT:
            alpha += 0.1; break;
            
        case GLUT_KEY_UP :
            beta += 0.1f;
            if (beta > 1.5f)
                beta = 1.5f;
            break;
            
        case GLUT_KEY_DOWN:
            beta -= 0.1f;
            if (beta < -1.5f)
                beta = -1.5f;
            break;
            
        case GLUT_KEY_PAGE_UP : radius -= 0.1f;
            if (radius < 0.1f)
                radius = 0.1f;
            break;
            
        case GLUT_KEY_PAGE_DOWN: radius += 0.1f; break;
            
    }
    sphericalToCartesian();
    glutPostRedisplay();
}


int main(int argc, char **argv) {
    
    // initialization
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(320,320);
    glutCreateWindow("CG@DI-UM");
    
    // callback registry 
    glutDisplayFunc(renderScene);
    glutIdleFunc(renderScene);
    glutReshapeFunc(changeSize);
    glutSpecialFunc(processKeys);
    
    // OpenGL settings 
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    
    // init
    sphericalToCartesian();
    prepareCylinder(2,1,10);
    
    // enter GLUTs main cycle
    glutMainLoop();
    
    return 0;
}
